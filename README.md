# Why people SMOTE

**ATOM is meeting on Tuesday, 30th January, 6:30pm, at Galvanize!**

Join us for a highly participative discussion !

Evan Adkins will be leading a discussion on the Synthetic Minority Over-sampling Technique (https://arxiv.org/abs/1106.1813) ( SMOTE ). He asks, "why would you ever make up fake data, and why does it work when you do?" SMOTE is a technique for dealing with building classifiers when the dataset has imbalance in its classes.

Further Reading:
Imbalanced data: Empirical results and current trends:
http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.413.1919&rep=rep1&type=pdf
(some interesting points around pg 16)

SMOTE on big data:  
http://www.sciencedirect.com/science/article/pii/S0020025514003272?via%3Dihub    

https://www.semanticscholar.org/paper/On-the-use-of-MapReduce-for-imbalanced-big-data-us-R%C3%ADo-L%C3%B3pez/2c9cb9d1aa1f4830ed2841097edd4de6c7ef3ff9

Comparison of various sampling methods:  
http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.58.7757&rep=rep1&type=pdf

Turns out good old fashioned regularization is kinda like adding fake data too:
http://madrury.github.io/jekyll/update/statistics/2017/08/12/noisy-regression.html

Even Further Reading:  
https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-14-106

https://www.researchgate.net/publication/282878882_Synthetic_Oversampling_for_Advanced_Radioactive_Threat_Detection

Also of potential interest for visualizing outputs of decision tree and random forest models (we <3 JVP):  
https://jakevdp.github.io/PythonDataScienceHandbook/05.08-random-forests.html

About ATOM:  
Advanced Topics on Machine learning ( ATOM ) is a learning and discussion group for cutting-edge machine learning techniques in the real world. We work through winning Kaggle competition entries or real-world ML projects, learning from those who have successfully applied sophisticated data science pipelines to complex problems.

As a discussion group, we strongly encourage participation, so be sure to read up about the topic of conversation beforehand !

ATOM can be found on PuPPy’s Slack under the channel #atom, and on PuPPy’s Meetup.com events.

We're kindly hosted by Galvanize (https://www.galvanize.com). Thank you !